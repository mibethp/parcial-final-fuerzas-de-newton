import math
def fuerzas_newton(θ,μ,m1):

    resultados=[]
    
    g= 9.8
    m2=0
    a=-1
    while a<0:
        a=math.ceil(g*((m2-(m1*math.sin(math.radians(θ)))-(m1*μ*math.cos(math.radians(θ))))/(m1+m2)))
        if a<0:
            resultados.append ([m2,'subiendo'])
            m2=m2+0.5
        else:
            resultados.append([m2,'bajando'])
    return resultados

d=fuerzas_newton(55,0.15,6)
print(d)

def angulo_equilibrio(m1,m2,μ):
    masas = m2/m1
    try:
        angsin1 = (masas+(μ*math.sqrt(1-math.pow(masas,2)+math.pow(μ,2))))/(1+math.pow(μ,2))
        angsin2 = (masas-(μ*math.sqrt(1-math.pow(masas,2)+math.pow(μ,2))))/(1+math.pow(μ,2))
        angulo1 = math.degrees(math.asin(angsin1))
        angulo2 = math.degrees(math.asin(angsin2))
    except ValueError:
        return -1
        
    if 85>angulo1>10 and 85>angulo2>10: return angulo1,angulo2
    elif 85>angulo1>10:  return angulo1
    elif 85>angulo2>10:  return angulo2
    else: return -1

p= angulo_equilibrio (5,6,0.3)
print(p)